package server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementa��o propriamente dita do motor de execu��o como uma implementa��o
 * da interface remota {@link server.Compute Compute}
 * @author Everton Cavalcante
 */
public class ComputeEngine extends UnicastRemoteObject implements Compute {
	/** <em>Default serial ID</em> */
	private static final long serialVersionUID = 1L;
	
	/** Nome a ser atribu�do ao objeto remoto que representa o motor de computa��o */
	private static final String NAME = "ComputeEngine";

	/** 
	 * Construtor padr�o
	 * @throws java.rmi.RemoteException Exce��o decorrente de falha na invoca��o do m�todo
	 */
	public ComputeEngine() throws RemoteException {
		super();
	}
	
	/**
	 * Implementa��o do m�todo respons�vel pela execu��o da tarefa submetida ao
	 * motor de execu��o, conforme definido na interface remota 
	 * {@link server.Compute Compute}
	 * @param task Refer�ncia para a tarefa a ser executada
	 * @throws java.rmi.RemoteException Exce��o decorrente de falha na invoca��o do m�todo
	 */
	@Override
	public <T> T execute(Task<T> task) throws RemoteException {
		System.out.println("Executing task " + task.getClass().getName());
		return task.execute();
	}
	
	/** 
	 * M�todo principal
	 * @param args Argumentos de linha de comando
	 */
	public static void main(String[] args) {
		try {
			// Instancia��o do RMI Registry na porta 1099
			LocateRegistry.createRegistry(1099);
			
			// Instancia��o do objeto remoto (stub) referente ao motor de execu��o
			Compute engine = new ComputeEngine();
			
			// Registro do objeto remoto no RMI Registry
			Naming.rebind("rmi://localhost/" + NAME, engine);
			System.out.println(NAME + " bound at RMI Registry.");
		} catch (RemoteException | MalformedURLException e) {
			System.err.println(NAME + " exception:");
			e.printStackTrace();
		}
	}
}
