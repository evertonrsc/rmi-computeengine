package server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface remota do motor de computa��o
 * @author Everton Cavalcante
 */
public interface Compute extends Remote {
	/** 
	 * M�todo gen�rico atrav�s do qual as tarefas ser�o submetidas para execu��o 
	 * no servidor
	 * @param <T> Tipo de retorno da tarefa executada
	 * @param task Refer�ncia para a tarefa a ser executada
	 * @return Resultado da execu��o da tarefa em quest�o
	 * @throws java.rmi.RemoteException Exce��o decorrente de falha na invoca��o do m�todo  
	 */
	<T> T execute(Task<T> task) throws RemoteException;
}
