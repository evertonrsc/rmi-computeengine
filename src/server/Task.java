package server;

/**
 * Interface gen�rica, n�o-remota, que define a estrutura da tarefa a ser 
 * executada pelo motor de computa��o 
 * @author Everton Cavalcante
 * @param <T> Tipo de retorno da tarefa executada
 */
public interface Task<T> {
	/**
	 * M�todo cuja implementa��o corresponder� �s instru��es a serem executadas 
	 * por uma tarefa
	 * @return Retorno da tarefa executada
	 */
	T execute();
}
