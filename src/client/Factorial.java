package client;

import java.io.Serializable;

import server.Task;

/**
 * Classe que prov� a implementa��o do c�lculo do fatorial de um n�mero
 * por meio da implementa��o da interface gen�rica
 * {@link server.Task Task}
 * @author Everton Cavalcante
 */
public class Factorial implements Task<Long>, Serializable {
	/** <em>Default serial ID</em> */
	private static final long serialVersionUID = 1L;
	
	/** N�mero cujo fatorial ser� calculado */
	private final int number;
	
	/**
	 * Construtor parametrizado
	 * @param number N�mero cujo fatorial ser� calculado
	 */
	public Factorial(int number) {
		this.number = number;
	}

	/**
	 * Retorna o n�mero cujo fatorial ser� calculado
	 * @return N�mero cujo fatorial ser� calculado
	 */
	public int getNumber() {
		return number;
	}
	
	/**
	 * Realiza o c�lculo do fatorial do n�mero em quest�o
	 * @return Fatorial do n�mero em quest�o
	 */
	@Override
	public Long execute() {
		return factorial(this.number);
	}
	
	/**
	 * Calcula o fatorial de um n�mero de forma recursiva
	 * @param number N�mero cujo fatorial ser� calculado
	 * @return Fatorial do n�mero em quest�o
	 */
	private long factorial(int number) {
		if (number == 0) {
			return 1;
		} else {
			return number * factorial(number-1);
		}
	}
}
