package client;

import java.io.Serializable;

import server.Task;

/**
 * Classe que prov� a implementa��o do c�lculo de &pi;
 * por meio da implementa��o da interface gen�rica
 * {@link server.Task Task}.<br>
 * Dependendo do m�todo utilizado para o c�lculo de &pi;, esta classe 
 * poder� conter diferentes atributos necess�rios a essa 
 * computa��o, mas ter� pelo menos um atributo referente � 
 * precis�o com a qual o valor de &pi; ser� calculado.
 * @author Everton Cavalcante
 */
public class Pi implements Task<Double>, Serializable {
	/** <em>Default serial ID</em> */
	private static final long serialVersionUID = 1L;
	
	/** Precis�o da aproxima��o do valor de &pi; a ser calculado */
	private final int precision;
	
	/**
	 * Construtor parametrizado
	 * @param precision Precis�o da aproxima��o
	 */
	public Pi(int precision) {
		this.precision = precision;
	}

	/**
	 * Retorna a precis�o especificada para o c�lculo de &pi;
	 * @return Precis�o da aproxima��o
	 */
	public int getPrecision() {
		return precision;
	}
	
	/**
	 * Realiza o c�lculo do valor aproximado de &pi; por meio da 
	 * <a href="https://en.wikipedia.org/wiki/Leibniz_formula_for_pi">
	 * s�rie infinita de Leibniz</a>
	 * @return Valor aproximado de &pi;
	 */
	@Override
	public Double execute() {
		double sum = 0.0;
		for (int i = 0; i < precision; i++) {
			sum += computeTerm(i);
		}
		return sum;
	}
	
	/**
	 * Calcula o valor de um determinado termo da s�rie de Leibniz
	 * @param index �ndice do termo da serie a ser calculado
	 * @return Valor do termo
	 */
	private double computeTerm(int index) {
		double term = 4 * Math.pow(-1, index) / (2 * index + 1); 
		return term;
	}
}
