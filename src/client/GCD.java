package client;

import java.io.Serializable;

import server.Task;

/**
 * Classe que prov� a implementa��o do c�lculo do m�ximo divisor comum (MDC) entre
 * dois n�meros inteiros por meio da implementa��o da interface gen�rica
 * {@link server.Task Task}
 * @author Everton Cavalcante
 */
public class GCD implements Task<Integer>, Serializable {
	/** <em>Default serial ID</em> */
	private static final long serialVersionUID = 1L;
	
	/** Primeiro n�mero objeto do c�lculo */
	private final int number1;
	
	/** Segundo n�mero objeto do c�lculo */
	private final int number2;
	
	/**
	 * Construtor parametrizado
	 * @param number1 Primeiro n�mero objeto do c�lculo
	 * @param number2 Segundo n�mero objeto do c�lculo
	 */
	public GCD(int number1, int number2) {
		this.number1 = number1;
		this.number2 = number2;
	}

	/**
	 * Retorna o primeiro n�mero objeto do c�lculo
	 * @return Primeiro n�mero objeto do c�lculo
	 */
	public int getNumber1() {
		return number1;
	}
	
	/**
	 * Retorna o segundo n�mero objeto do c�lculo
	 * @return Segundo n�mero objeto do c�lculo
	 */
	public int getNumber2() {
		return number2;
	}
	
	/**
	 * Realiza o c�lculo do m�ximo divisor comum entre os dois n�meros em quest�o
	 * utilizando o <a href="https://en.wikipedia.org/wiki/Euclidean_algorithm">
	 * Algoritmo de Euclides</a> em sua vers�o recursiva
	 * @return M�ximo divisor comum entre os dois n�meros em quest�o
	 */
	@Override
	public Integer execute() {
		return gcd(this.number1, this.number2);
	}
	
	/**
	 * Calcula o m�ximo divisor comum entre dois n�meros
	 * @param number1 Primeiro n�mero objeto do c�lculo
	 * @param number2 Segundo n�mero objeto do c�lculo
	 * @return M�ximo divisor comum entre os dois n�meros
	 */
	private int gcd(int number1, int number2) {
		if (number2 == 0) {
			return number1;
		} else {
			return gcd(number2, number1 % number2);
		}
	}
}
