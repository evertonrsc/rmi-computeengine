package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import server.Compute;

/**
 * Classe que representa o programa cliente para o c�lculo do fatorial de um n�mero
 * @author Everton Cavalcante
 */
public class ComputeFactorial {
	/** Nome do objeto remoto que representa o motor de computa��o */
	private static final String NAME = "ComputeEngine";
	
	/**
	 * M�todo principal
	 * @param args Argumentos de linha de comando
	 */
	public static void main(String[] args) {
		try {
			/* Obten��o de refer�ncia para o objeto remoto ComputeEngine 
			 * por meio de consulta ao RMI Registry, executado por padr�o 
			 * na porta 1099 */
			Compute computeEngine = (Compute) Naming.lookup("rmi://localhost/" + NAME);
			
			// N�mero cujo fatorial ser� calculado
			int number = Integer.parseInt(args[0]);
			
			// Instancia��o da tarefa a ser executada
			Factorial taskFactorial = new Factorial(number);
						
			// Invoca��o do m�todo remoto atrav�s da refer�ncia obtida para o objeto remoto
			long result = computeEngine.execute(taskFactorial);
			System.out.println("Factorial of " + number + ": " + result);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.err.println(NAME + " exception:");
			e.printStackTrace();
		}
	}
}
