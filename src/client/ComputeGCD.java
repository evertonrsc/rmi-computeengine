package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import server.Compute;

/**
 * Classe que representa o programa cliente para o c�lculo do m�ximo divisor 
 * comum entre dois n�meros
 * @author Everton Cavalcante
 */
public class ComputeGCD {
	/** Nome do objeto remoto que representa o motor de computa��o */
	private static final String NAME = "ComputeEngine";
	
	/**
	 * M�todo principal
	 * @param args Argumentos de linha de comando
	 */
	public static void main(String[] args) {
		try {
			/* Obten��o de refer�ncia para o objeto remoto ComputeEngine 
			 * por meio de consulta ao RMI Registry, executado por padr�o 
			 * na porta 1099 */
			Compute computeEngine = (Compute) Naming.lookup("rmi://localhost/" + NAME);
			
			// N�meros dos quais ser� calculado o m�ximo divisor comum
			int number1 = Integer.parseInt(args[0]);
			int number2 = Integer.parseInt(args[1]);
			
			// Instancia��o da tarefa a ser executada
			GCD taskGCD = new GCD(number1, number2);
						
			// Invoca��o do m�todo remoto atrav�s da refer�ncia obtida para o objeto remoto
			long result = computeEngine.execute(taskGCD);
			System.out.println("Greatest common divisor between " + number1 + 
				" and " + number2 + ": " + result);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.err.println(NAME + " exception:");
			e.printStackTrace();
		}
	}
}
