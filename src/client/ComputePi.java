package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import server.Compute;

/**
 * Classe que representa o programa cliente para o c�lculo de &pi;
 * @author Everton Cavalcante
 */
public class ComputePi {
	/** Nome do objeto remoto que representa o motor de computa��o */
	private static final String NAME = "ComputeEngine";
	
	/**
	 * M�todo principal
	 * @param args Argumentos de linha de comando
	 */
	public static void main(String[] args) {
		try {
			/* Obten��o de refer�ncia para o objeto remoto ComputeEngine 
			 * por meio de consulta ao RMI Registry, executado por padr�o 
			 * na porta 1099 */
			Compute computeEngine = (Compute) Naming.lookup("rmi://localhost/" + NAME);
			
			// Precis�o do c�lculo, fornecida via linha de comando
			int precision = Integer.parseInt(args[0]);
			
			// Instancia��o da tarefa a ser executada
			Pi taskPi = new Pi(precision);
						
			// Invoca��o do m�todo remoto atrav�s da refer�ncia obtida para o objeto remoto
			double result = computeEngine.execute(taskPi);
			System.out.println("Value of pi with precision of " + precision + ": " + result);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.err.println(NAME + " exception:");
			e.printStackTrace();
		}
	}
}
